#!/bin/bash
#
# Script to safely add routing tables to /etc/iproute2/rt_tables
#
# Copyright (C) 2010 Jordi Casas Ríos 
# 
# This script is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option) any
# later version.
# 
# You should have received a copy of the GNU General Public License along with
# this program; If not, see <http://www.gnu.org/licenses/>
#
# Author: Jordi Casas Ríos <txorlings@gmail.com>
#

FILE=/etc/iproute2/rt_tables
REGEXP="linkat-lustitia"

sed -i "/${REGEXP}/d" $FILE

