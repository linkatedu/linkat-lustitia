#!/bin/bash
#
# Script to safely add routing tables to /etc/iproute2/rt_tables
#
# Copyright (C) 2010 Jordi Casas Ríos 
# 
# This script is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option) any
# later version.
# 
# You should have received a copy of the GNU General Public License along with
# this program; If not, see <http://www.gnu.org/licenses/>
#
# Author: Jordi Casas Ríos <txorlings@gmail.com>
#

# Load configuration.
. /etc/lustitia.conf


FILE=/etc/iproute2/rt_tables
COMMENT="# Added by linkat-lustitia, lines with 'linkat-lustitia' will be deleted on uninstall!"
FREE=""

for a in `seq 0 255` ; do 
	if ! grep ^$a[^0-9] $FILE > /dev/null 2>&1 ; then
		FREE="$FREE $a"
	fi
done
TABLES=($FREE)
for (( router=0; router<$N_ROUTERS; router++ )); do
	N=`expr $router + 1`
	echo -e "${TABLES[$router]}\tenlace${N}\t${COMMENT}" >> $FILE
done
