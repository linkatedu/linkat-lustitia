#!/bin/bash -x
# Autor: Victor Carceler
#
# Licencia: GPL v3 o posterior
#
# El propósito de este script es:
#
#  - Agregar una IP para monitorizar cada router
#  - Inicializar las tablas corresponientes a los routers (deben haberse
#    definido en /etc/iproute2/rt_tables previamente). Cada tabla tendrá la
#    ruta necesaria para alcanzar al router y la ruta por defecto
#  - Agregar reglas para utilizar la tabla correspondiente en función
#    de la dirección de origen del paquete (una de las IPs del primer punto)
#  - Fijar como ruta por defecto una ruta multihop utilizando todos los
#    routers
#  - Utilizar valores realm diferentes en cada enlace para poder moldear
#    el tráfico por router

# Modified by: Jordi "Txor" Casas Ríos <jordi.casas@opentrends.net>

# Load configuration.
. /etc/lustitia.conf

ETH_OUT_IP=`/sbin/ifconfig $ETH_OUT | grep "inet "  | sed 's/^ *//;s/ *$//' | cut -d " " -f 2`
ETH_OUT_MASK=`/sbin/ifconfig $ETH_OUT | grep "netmask " | sed 's/^ *//;s/ *$//' | cut -d " " -f 5`
ETH_IN_IP=`/sbin/ifconfig $ETH_IN | grep "inet "  | sed 's/^ *//;s/ *$//' | cut -d " " -f 2`
ETH_IN_MASK=`/sbin/ifconfig $ETH_IN | grep "netmask " | sed 's/^ *//;s/ *$//' | cut -d " " -f 5`

# WAN
ifconfig $ETH_OUT $ETH_OUT_IP netmask $ETH_OUT_MASK up

# LAN
ifconfig $ETH_IN $ETH_IN_IP netmask $ETH_IN_MASK up

# Clear default rules substrings
DEFAULT_RULE=""
for ((router=0; router<$N_ROUTERS; router++)); do
	# For each router enabled ...
	ENABLE="ROUTER_"$router"_ENABLE"
	if [ ${!ENABLE} -eq 1 ] ; then
		# Prepare variable names
		IP="ROUTER_"$router"_IP"
		IPMON="ROUTER_"$router"_IPMON"
		MASK="ROUTER_"$router"_MASK"
		WEIGHT="ROUTER_"$router"_WEIGHT"
		N=`expr $router + 1`
		ifconfig $ETH_OUT:$router ${!IPMON} netmask $ETH_OUT_MASK up
		IP_LIST=(${!IP//./ })
		MASK_LIST=(${!MASK//./ })
		NET=`awk -v ip1=${IP_LIST[0]} -v ip2=${IP_LIST[1]} -v ip3=${IP_LIST[2]} -v ip4=${IP_LIST[3]} -v mask1=${MASK_LIST[0]} -v mask2=${MASK_LIST[1]} -v mask3=${MASK_LIST[2]} -v mask4=${MASK_LIST[3]} 'BEGIN { print and(ip1,mask1)"."and(ip2,mask2)"."and(ip3,mask3)"."and(ip4,mask4); exit }'`
		ip route add $NET/${!MASK} dev $ETH_OUT table enlace$N
		ip route add default via ${!IP} table enlace$N
		ip rule add from ${!IPMON} table enlace$N
		DEFAULT_RULE="$DEFAULT_RULE nexthop via ${!IP} weight ${!WEIGHT} realm $N"
	fi
done

# Default rule
if [ "$DEFAULT_RULE" != "" ]
then
	ip route replace default scope global $DEFAULT_RULE
fi
